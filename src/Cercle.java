
public class Cercle implements Drawable {

	private Point centre;
	private double rayon;
	
	
	public Cercle() {
		// TODO Auto-generated constructor stub
	}
	
	public Cercle(Point centre, double rayon) {
		this.centre = centre;
		this.rayon = rayon;
	}
	
	public void draw(){
		System.out.println( "cercle(" + centre + ", " +  rayon + ")");
	}

}
