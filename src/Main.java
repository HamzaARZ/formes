
public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		/*Point A = new Point(0.0, 0.0);
		Point B = new Point(1.0, 0.0);
		Point C = new Point(3.0, 2.0);
		Point D = new Point(-1.0, 0.0);
		
		//Cercle cercle1 = new Cercle(A, 1.0);
		//Triangle triangle1 = new Triangle(A, B, D);
		Rectangle rectangle1 = new RectangleDetails(A, B, C, D, "rouge", "blue", 2.0);
		
		//System.out.println(A.calculDistance(B));
		
		//FormeUtils.draw(C);
		//FormeUtils.draw(cercle1);
		//FormeUtils.draw(triangle1);
		FormeUtils.draw(rectangle1);
		
		System.out.println("-------------");*/
		
		
		Alimentaire alimentaire1 = new Alimentaire("alim1", 10);
		Alimentaire alimentaire2 = new Alimentaire("alim2", 5);
		
		Electromenager electromenager1 = new Electromenager("ele1", 7);
		Electromenager electromenager2 = new Electromenager("ele2", 30);
		
		Produit[] produits = {alimentaire1, alimentaire2, electromenager1, electromenager2};
		
		double prixTotal = 0.0;
		for (Produit produit: produits){
			prixTotal += produit.calculePrix();
		}
		
		System.out.println(prixTotal);
	}
}
