
public class Triangle implements Drawable {

	private Point sommet1;
	private Point sommet2;
	private Point sommet3;
	
	
	public Triangle() {
		// TODO Auto-generated constructor stub
	}
	
	public Triangle(Point sommet1, Point sommet2, Point sommet3){
		this.sommet1 = sommet1;
		this.sommet2 = sommet2;
		this.sommet3 = sommet3;
	}

	
	
	public void draw(){
		System.out.println("triangle(" + sommet1 + ", " + sommet2 + ", "
				+ sommet3 + ")");
	}
}
