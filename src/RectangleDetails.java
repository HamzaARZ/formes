
public class RectangleDetails extends Rectangle{
	
	private String couleurInterne;
	private String couleurCadre;
	private double epaisseur;
	
	public RectangleDetails() {
		// TODO Auto-generated constructor stub
	}

	
	
	public RectangleDetails(Point sommet1, Point sommet2, Point sommet3, Point sommet4,
			String couleurInterne, String couleurCadre, double epaisseur) {
		
		super(sommet1, sommet2, sommet3, sommet4);
		this.couleurInterne = couleurInterne;
		this.couleurCadre = couleurCadre;
		this.epaisseur = epaisseur;
	}
	
	
	public void draw(){
		super.draw();
		System.out.println("\t--> couleur interne : " + couleurInterne + " - couleur du cadre : " + 
				couleurCadre + " - epaisseur = " + epaisseur);
	}
}
