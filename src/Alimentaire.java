
public class Alimentaire extends Produit{
	
	public static double TVA = 0.15;

	public Alimentaire() {
		// TODO Auto-generated constructor stub
	}
	
	public Alimentaire(String nom, int prix) {
		super(nom, prix);
	}
	
	public double calculePrix(){
		return(prix + TVA*prix);
	}

}
