
public class Point implements Drawable {

	
	private double x;
	private double y;
	
	public Point() {
		// TODO Auto-generated constructor stub
	}
	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public double calculDistance(Point A){
		return Math.sqrt(Math.pow(this.x - A.x, 2) + Math.pow(this.y - A.y, 2));
	}
	
	public void draw(){
		System.out.println("Point(" + x + ", " + y + ")");
	}
	
	public String toString(){
		return "Point(" + x + ", " + y + ")";
	}
}
